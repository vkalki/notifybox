package com.example.notifybox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterTask extends RecyclerView.Adapter<AdapterTask.MyHolder> {

    private Context context;
    private List<ModelTask> taskList;

    AdapterTask(Context context, List<ModelTask> taskList) {
        this.context = context;
        this.taskList = taskList;
    }

    @NonNull
    @Override
    public AdapterTask.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.task_horizontal, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTask.MyHolder holder, int position) {

        String taskTitle = taskList.get(position).getTitle();
        String taskText = taskList.get(position).getSubtext();
        String taskState = taskList.get(position).getState();
        String timestamp = taskList.get(position).getTimestamp();

        holder.taskTitleTv.setText(taskTitle);
        holder.taskTextTv.setText(taskText);
        holder.taskStateTv.setText(taskState);
        holder.taskDateTv.setText(timestamp);

        //HANDLE ITEM CLICKS
        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        }*/
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    void addItem(ModelTask task) {
        taskList.add(task);
        notifyItemInserted(taskList.size());
    }

    void removeItem(int position) {
        taskList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, taskList.size());
    }

    //internal view holder class (bean)
    static class MyHolder extends RecyclerView.ViewHolder {

        TextView taskTextTv, taskTitleTv, taskStateTv, taskDateTv;

        MyHolder(@NonNull View itemView) {
            super(itemView);

            taskTitleTv = itemView.findViewById(R.id.task_title_tv);
            taskTextTv = itemView.findViewById(R.id.task_text_tv);
            taskStateTv = itemView.findViewById(R.id.task_state_tv);
            taskDateTv = itemView.findViewById(R.id.task_date_tv);

        }
    }
}
