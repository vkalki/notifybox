package com.example.notifybox;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class DialogTask extends DialogFragment {

    public interface OnInputSelected {
        void sendInput(String title, String text);
    }

    private OnInputSelected onInputSelected;
    private TextInputEditText titleEt, textEt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_task, container, false);

        titleEt = view.findViewById(R.id.task_title_et);
        textEt = view.findViewById(R.id.task_text_et);
        MaterialButton actionAdd = view.findViewById(R.id.action_add);
        MaterialButton actionCancel = view.findViewById(R.id.action_cancel);

        actionAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleEt.getText().toString().isEmpty()) {
                    titleEt.setHint("A title is required!");
                    titleEt.setHintTextColor(Color.RED);

                } else {
                    onInputSelected.sendInput(titleEt.getText().toString(), textEt.getText().toString());
                    Objects.requireNonNull(getDialog()).dismiss();
                }

            }
        });

        actionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getDialog()).dismiss();
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        onInputSelected = (OnInputSelected) getTargetFragment();
    }
}
