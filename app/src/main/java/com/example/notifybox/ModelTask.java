package com.example.notifybox;

public class ModelTask {

    private String title, subtext, state, timestamp;

    public ModelTask() {

    }

    ModelTask(String title, String subtext, String state, String timestamp) {
        this.title = title;
        this.subtext = subtext;
        this.state = state;
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtext() {
        return subtext;
    }

    public void setSubtext(String subtext) {
        this.subtext = subtext;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
