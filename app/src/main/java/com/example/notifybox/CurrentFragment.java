package com.example.notifybox;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CurrentFragment extends Fragment implements DialogTask.OnInputSelected {

    private List<ModelTask> taskList;
    private AdapterTask adapterTask;
    private RecyclerView recyclerView;
    private Paint p = new Paint();

    private FirebaseUser user;

    public CurrentFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_current, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();

        recyclerView = view.findViewById(R.id.current_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapterTask = new AdapterTask(getActivity(), taskList);
        recyclerView.setAdapter(adapterTask);

        readTasks();
        initSwipe();

        return view;
    }

    private void readTasks() {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("currentTasks");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                taskList.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    taskList.add(snapshot.getValue(ModelTask.class));
                    recyclerView.setAdapter(new AdapterTask(getActivity(), taskList));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true); //show menu option in fragment
        super.onCreate(savedInstanceState);

        if (taskList == null) {
            taskList = new ArrayList<>();
        }
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    deleteTask(position);
                    adapterTask.removeItem((position));
                } else if (direction == ItemTouchHelper.RIGHT) {
                    archiveTask(position);
                    deleteTask(position);
                    adapterTask.removeItem(position);
                }
            }

            private void archiveTask(int position) {
                FirebaseDatabase.getInstance()
                        .getReference("Users")
                        .child(user.getUid())
                        .child("savedTasks")
                        .child(taskList.get(position).getTitle())
                        .setValue(new ModelTask(
                                taskList.get(position).getTitle(),
                                taskList.get(position).getSubtext(),
                                "Archived:", getTime()));
            }

            private void deleteTask(int position) {
                FirebaseDatabase.getInstance()
                        .getReference("Users")
                        .child(user.getUid())
                        .child("currentTasks")
                        .child(taskList.get(position).getTitle())
                        .removeValue();
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    if (dX > 0) {
                        p.setColor(Color.parseColor("#388E3C"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        //inflate menu
        inflater.inflate(R.menu.menu_main, menu);

        MenuItem addItem = menu.findItem(R.id.action_add);
        addItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                DialogTask dialog = new DialogTask();
                dialog.setTargetFragment(CurrentFragment.this, 1);

                assert getFragmentManager() != null;
                dialog.show(getFragmentManager(), "DialogTask");

                return true;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);
    }

    // capture input
    @Override
    public void sendInput(String title, String text) {

        String timestamp = getTime();
        String state = "Created:";

        taskList.add(new ModelTask(title, text, state, timestamp));

        FirebaseDatabase.getInstance()
                .getReference("Users")
                .child(user.getUid())
                .child("currentTasks")
                .child(title)
                .setValue(new ModelTask(title, text, state, timestamp));


    }

    private String getTime() {
        Calendar cal = Calendar.getInstance();
        return DateFormat.format("MM/dd/yy", cal).toString();
    }
}
