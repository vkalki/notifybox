package com.example.notifybox;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

public class DashboardActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // display the navigation bar
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(selectedListener);

        // on activity launch, open the current task fragment
        CurrentFragment currentFragment = new CurrentFragment();
        FragmentTransaction currentTransaction = getSupportFragmentManager().beginTransaction();
        currentTransaction.replace(R.id.content, currentFragment, "");
        currentTransaction.commit();
    }

    // handle navigation page changes
    private BottomNavigationView.OnNavigationItemSelectedListener selectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_current:
                    CurrentFragment currentFragment = new CurrentFragment();
                    FragmentTransaction currentTransaction = getSupportFragmentManager().beginTransaction();
                    currentTransaction.replace(R.id.content, currentFragment, "");
                    currentTransaction.commit();
                    return true;

                case R.id.nav_saved:
                    SavedFragment savedFragment = new SavedFragment();
                    FragmentTransaction savedTransaction = getSupportFragmentManager().beginTransaction();
                    savedTransaction.replace(R.id.content, savedFragment, "");
                    savedTransaction.commit();
                    return true;
            }
            return false;
        }
    };


}
