package com.example.notifybox;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

/**
 * Handle user registration with the service. If the user is already registered, prompt the
 * user to utilize the user login page instead. If the user is not already registered, ask
 * for login information that can be used to identify the user within the Firebase Database.
 */
public class RegisterActivity extends AppCompatActivity {

    //views
    private EditText mEmailEt, mPasswordEt;

    //firebase auth
    private FirebaseAuth mAuth;

    //progressbar to show to the user while registering
    private ProgressDialog progressDialog;

    /**
     * Initialize on startup
     *
     * @param savedInstanceState store the prior page the user was on
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //ActionBar and its title
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Create Account");

        //enable back button
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        //init
        mEmailEt = findViewById(R.id.emailEt);
        mPasswordEt = findViewById(R.id.passwordEt);
        Button mRegisterBtn = findViewById(R.id.registerBtn);
        TextView mHaveAccountTv = findViewById(R.id.have_accountTv);

        //init firebase
        mAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering User...");

        //handle register button
        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //input email, password
                String email = mEmailEt.getText().toString().trim();
                String password = mPasswordEt.getText().toString().trim();

                //validate
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mEmailEt.setError("Invalid Email");
                    mEmailEt.setFocusable(true);
                } else if (password.length() < 6) {
                    mPasswordEt.setError("Password length must be at least 6 characters");
                    mPasswordEt.setFocusable(true);
                } else {
                    registerUser(email, password);
                }
            }
        });

        //handle login text view lick listener
        mHaveAccountTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });

    }

    /**
     * Handle user registration
     *
     * @param email    the email of the target user
     * @param password the password (hidden) of the target user
     */
    private void registerUser(String email, String password) {
        //When valid, register the user
        progressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, dismiss dialog and start registration
                            progressDialog.dismiss();
                            FirebaseUser user = mAuth.getCurrentUser();


                            String email = user.getEmail();
                            String uid = user.getUid();
                            //when the user is registered, store info in the realtime firebase database
                            HashMap<Object, Object> userData = new HashMap<>();
                            //put info into the hashmap
                            userData.put("email", email);
                            userData.put("uid", uid);

                            //create firebase instance
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            //user reference
                            DatabaseReference reference = database.getReference("Users").child(uid);
                            //put data within hashmap in database

                            reference.setValue(userData);

                            Toast.makeText(RegisterActivity.this, "Registered...\n" + user.getEmail(), Toast.LENGTH_SHORT).show();

                            //open profile and complete the registration process
                            startActivity(new Intent(RegisterActivity.this, DashboardActivity.class));
                            finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            progressDialog.dismiss();
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //on failure
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    /**
     * Enable back button support
     *
     * @return the previous activity
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed(); //go to previous activity
        return super.onSupportNavigateUp();
    }

}
